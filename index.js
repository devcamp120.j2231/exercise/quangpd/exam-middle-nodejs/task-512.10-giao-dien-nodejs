//import thư viện express
const express = require("express");
const path = require("path");

//Khởi tạo app express
const app = express();

//Khai báo cổng 
const port = 8000;

//Middle Ware in ra thời gian hiện tại
const timeNowMiddleWare = (req,res,next) =>{
    console.log("Time now is :");
    console.log(new Date());
    next();
}

//Middle Ware in ra url
const urlIsMiddleWare = (req,res,next) =>{
    console.log("Url you just Insert is : localhost:8000/");
    next();
}
app.use(urlIsMiddleWare);

app.get("/",timeNowMiddleWare,(req,res) =>{
    console.log(__dirname);
    res.sendFile(path.join(__dirname + "/views/index.html"));
});
app.use(express.static(__dirname + "/views"));

app.listen(port,()=>{
    console.log("App listen on port : " , port);
})